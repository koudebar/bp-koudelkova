﻿//----------------------------------------------------------------------------------------
/**
 * \file    ControllerHandler.cs
 * \author  Barbora Koudelková
 * \date    May 2022
 * \brief   Shows Player´s hands with controllers. Debug purposes only.
 */
 //----------------------------------------------------------------------------------------
 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
public class ControllerHandler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach(var hand in Player.instance.hands){
            hand.ShowController();
            hand.SetSkeletonRangeOfMotion(Valve.VR.EVRSkeletalMotionRange.WithController);
        }
    }
}
