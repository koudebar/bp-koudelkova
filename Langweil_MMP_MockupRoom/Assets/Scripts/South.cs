﻿//----------------------------------------------------------------------------------------
/**
 * \file    South.cs
 * \author  Barbora Koudelková
 * \date    May 2022
 * \brief   Videoplayer handler for southern projection. Due to Unity ResourceLoader tasks order this cannot be
 *          achieved using one script only for all the videos resulting in latency of synchronous projections. 
 */
 //----------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Video;
public class South : MonoBehaviour
{
    public VideoPlayer vidSource;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    // If PlayerPref variable selected by Player is true, play desired chapter and reset it again.
    void Update()
    {
        if (PlayerPrefs.GetInt("chapter1S") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("1_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter1S", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter2S") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("2_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter2S", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter3S") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("3_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter3S", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter4S") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("4_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter4S", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter5S") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("5_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter5S", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter6S") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("6_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter6S", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter7S") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("7_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter7S", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter8S") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("8_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter8S", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter9S") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("9_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter9S", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter10S") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("10_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter10S", 0);
        }
    }
}
