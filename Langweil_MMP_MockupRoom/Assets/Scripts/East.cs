﻿//----------------------------------------------------------------------------------------
/**
 * \file    East.cs
 * \author  Barbora Koudelková
 * \date    May 2022
 * \brief   Videoplayer handler for eastern projection. Due to Unity ResourceLoader tasks order this cannot be
 *          achieved using one script only for all the videos resulting in latency of synchronous projections. 
 */
 //----------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Video;
public class East : MonoBehaviour
{
    public VideoPlayer vidSource;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    // If PlayerPref variable selected by Player is true, play desired chapter and reset it again.
    void Update()
    {
        if (PlayerPrefs.GetInt("chapter1E") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("1_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter1E", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter2E") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("2_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter2E", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter3E") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("3_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter3E", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter4E") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("4_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter4E", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter5E") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("5_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter5E", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter6E") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("6_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter6E", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter7E") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("7_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter7E", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter8E") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("8_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter8E", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter9E") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("9_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter9E", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter10E") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("10_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter10E", 0);
        }
    }
}
