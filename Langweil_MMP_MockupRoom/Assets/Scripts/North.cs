﻿//----------------------------------------------------------------------------------------
/**
 * \file    North.cs
 * \author  Barbora Koudelková
 * \date    May 2022
 * \brief   Videoplayer handler for northern projection. Due to Unity ResourceLoader tasks order this cannot be
 *          achieved using one script only for all the videos resulting in latency of synchronous projections. 
 */
 //----------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Video;
public class North : MonoBehaviour
{
    public VideoPlayer vidSource;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    // If PlayerPref variable selected by Player is true, play desired chapter and reset it again.
    void Update()
    {
        if (PlayerPrefs.GetInt("chapter1N") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("1_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter1N", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter2N") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("2_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter2N", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter3N") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("3_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter3N", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter4N") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("4_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter4N", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter5N") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("5_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter5N", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter6N") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("6_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter6N", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter7N") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("7_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter7N", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter8N") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("8_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter8N", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter9N") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("9_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter9N", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter10N") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("10_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter10N", 0);
        }
    }
}
