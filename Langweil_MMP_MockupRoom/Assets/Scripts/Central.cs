﻿//----------------------------------------------------------------------------------------
/**
 * \file    Central.cs
 * \author  Barbora Koudelková
 * \date    May 2022
 * \brief   Videoplayer handler for central projection. Due to Unity ResourceLoader tasks order this cannot be
 *          achieved using one script only for all the videos resulting in latency of synchronous projections. 
 */
 //----------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Video;

public class Central : MonoBehaviour
{
    public VideoPlayer vidSource;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    // If PlayerPref variable selected by Player is true, play desired chapter and reset it again.
    void Update()
    {
        if (PlayerPrefs.GetInt("chapter1C") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("1_Central");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter1C", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter2C") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("2_Central");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter2C", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter3C") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("3_Central");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter3C", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter4C") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("4_Central");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter4C", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter5C") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("5_Central");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter5C", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter6C") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("6_Central");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter6C", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter7C") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("7_Central");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter7C", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter8C") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("8_Central");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter8C", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter9C") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("9_Central");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter9C", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter10C") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("10_Central");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter10C", 0);
        }
    }
}
