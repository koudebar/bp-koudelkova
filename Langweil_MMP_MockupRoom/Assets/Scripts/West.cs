﻿//----------------------------------------------------------------------------------------
/**
 * \file    West.cs
 * \author  Barbora Koudelková
 * \date    May 2022
 * \brief   Videoplayer handler for western projection. Due to Unity ResourceLoader tasks order this cannot be
 *          achieved using one script only for all the videos resulting in latency of synchronous projections. 
 */
 //----------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Video;
public class West : MonoBehaviour
{
    public VideoPlayer vidSource;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    // If PlayerPref variable selected by Player is true, play desired chapter and reset it again.
    void Update()
    {
        if (PlayerPrefs.GetInt("chapter1W") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("1_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter1W", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter2W") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("2_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter2W", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter3W") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("3_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter3W", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter4W") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("4_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter4W", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter5W") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("5_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter5W", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter6W") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("6_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter6W", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter7W") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("7_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter7W", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter8W") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("8_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter8W", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter9W") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("9_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter9W", 0);
        }
        
        else if (PlayerPrefs.GetInt("chapter10W") == 1)
        {
            vidSource.clip = Resources.Load<VideoClip>("10_Panorama");
            vidSource.Play();
            PlayerPrefs.SetInt("chapter10W", 0);
        }
    }
}
