﻿//----------------------------------------------------------------------------------------
/**
 * \file    Chapter.cs
 * \author  Barbora Koudelková
 * \date    May 2022
 * \brief   VR and canvas interactions handler. Scene-wide.
 */
 //----------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.Extras;
using UnityEngine.Video;


public class Chapter : MonoBehaviour
{
    
    public SteamVR_LaserPointer laserPointer;
    public Canvas canvo;

    //Awake is called when the script instance is being loaded
    //Contains the three input functions for laser pointer, extended.
    void Awake()
    {
        laserPointer.PointerIn += PointerInside;
        laserPointer.PointerOut += PointerOutside;
        laserPointer.PointerClick += PointerClick;
    }

    // Start is called before the first frame update
    void Start(){
        //Because Unity remembers PlayerPrefs integer values even after turning the app off, 
        //it is necessary to always set it to default values before the first frame update.
        PlayerPrefs.SetInt("GrabEnabled", 1);
        for(int i=1; i<11; i++){
           PlayerPrefs.SetInt("chapter"+i+"N", 0);
           PlayerPrefs.SetInt("chapter"+i+"S", 0);
           PlayerPrefs.SetInt("chapter"+i+"E", 0);
           PlayerPrefs.SetInt("chapter"+i+"W", 0);
           PlayerPrefs.SetInt("chapter"+i+"C", 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Checks if player pressed GrabGrip to open or close menu, if true, opens or closes the dialogue menu
        if (SteamVR_Actions._default.GrabGrip[SteamVR_Input_Sources.RightHand].stateDown && !canvo.enabled)
        {
            canvo.enabled = true;
        }
        else if (SteamVR_Actions._default.GrabGrip[SteamVR_Input_Sources.RightHand].stateDown && canvo.enabled){

            canvo.enabled = false;
        }

    }
    
    // Check if SteamVR Laser Pointer is colliding with canvas textbox UI´s box collider
    // If true, outlines the object.
    public void PointerInside(object sender, PointerEventArgs e)
    {
        if (e.target.name == "Kapitola1")
        {   
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
        }
        else if (e.target.name == "Kapitola2")
        {   
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
        }
        else if (e.target.name == "Kapitola3")
        {   
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
        }
        else if (e.target.name == "Kapitola4")
        {   
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
        }
        else if (e.target.name == "Kapitola5")
        {   
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
        }
        else if (e.target.name == "Kapitola6")
        {   
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
        }
         else if (e.target.name == "Kapitola7")
        {   
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
        }
         else if (e.target.name == "Kapitola8")
        {   
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
        }
         else if (e.target.name == "Kapitola9")
        {   
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
        }
         else if (e.target.name == "Kapitola10")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
        }
        else if (e.target.name == "Exit")
        {   
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.red;
            //Debug.Log("Ende");
        }
    }

    // Checks if SteamVR Laser Pointer is  not in collision with canvas textbox UI´s box collider
    // If not, sets the textbox´s color to white.
    public void PointerOutside(object sender, PointerEventArgs e)
    {
       
        if (e.target.name == "Kapitola1")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
        else if (e.target.name == "Kapitola2")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
        else if (e.target.name == "Kapitola3")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
        else if (e.target.name == "Kapitola4")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
        else if (e.target.name == "Kapitola5")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
        else if (e.target.name == "Kapitola6")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
        else if (e.target.name == "Kapitola7")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
        else if (e.target.name == "Kapitola8")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
        else if (e.target.name == "Kapitola9")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
         else if (e.target.name == "Kapitola10")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
        else if (e.target.name == "Exit")
        {
            Button button = e.target.gameObject.GetComponent<Button>();
            button.image.color = Color.white;
        }
        
    }
    
    // Check if SteamVR Laser Pointer is colliding with canvas textbox UI´s box collider and Player selected and pressed GrabPinch.
    // If true, sets the PlayerPref value corresponding to Canvas Textbox´s text value and closes the dialogue.
    public void PointerClick(object sender, PointerEventArgs e)
    {

        if(e.target.name == "Kapitola1")
        {
            SetProjection(1);
            canvo.enabled = false;
        }
        else if(e.target.name == "Kapitola2")
        {
            SetProjection(2);
            canvo.enabled = false;
        }
        else if(e.target.name == "Kapitola3")
        {
            SetProjection(3);
            canvo.enabled = false;
        }
        else if(e.target.name == "Kapitola4")
        {
            SetProjection(4);
            canvo.enabled = false;
        }
        else if(e.target.name == "Kapitola5")
        {
            SetProjection(5);
            canvo.enabled = false;
        }
        else if(e.target.name == "Kapitola6")
        {
            SetProjection(6);
            canvo.enabled = false;
        }
        else if(e.target.name == "Kapitola7")
        {
            SetProjection(7);
            canvo.enabled = false;
        }
        else if(e.target.name == "Kapitola8")
        {
            SetProjection(8);
            canvo.enabled = false;
        }
        else if(e.target.name == "Kapitola9")
        {
            SetProjection(9);
            canvo.enabled = false;
        }
        else if(e.target.name == "Kapitola10")
        {
            SetProjection(10);
            canvo.enabled = false;
        }
        else if(e.target.name == "Exit")
        {
            Application.Quit();
        }
        
    }

    //Sends information to NSEWC projections to start videoplayer.
    public void SetProjection(int i)
    {
        PlayerPrefs.SetInt("chapter"+i+"N", 1);
        PlayerPrefs.SetInt("chapter"+i+"S", 1);
        PlayerPrefs.SetInt("chapter"+i+"E", 1);
        PlayerPrefs.SetInt("chapter"+i+"W", 1);
        PlayerPrefs.SetInt("chapter"+i+"C", 1);
    }
}
