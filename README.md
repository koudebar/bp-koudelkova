# BP Koudelková

**Z důvodu zákazu veřejného sdílení audiovizuálního materiálu z projektu MMP jsou veškerá videa v tomto repozitáři brána jako placeholder. Pro obdržení dat pro videoprojekci Langweil kontaktujte vlastníka repozitáře, případně jsou na vyžádání u vedoucího práce, Ing. Davida Sedláčka, Ph.D.**

Možnost prohlížení ve virtuálně-realitní místnosti. Vizualizace pro MMP.

Tento projekt slouží pro vizualizaci promítaní v rámci konzultací se členy Muzea města Prahy.
V rootu je pak postup implementace mého bakalářského projektu a závěrečná dokumentace.

Projekt předpokládá vlastnictví Windows Mixed Reality Headset a pravého kontroleru.

## Návod na spuštění:
<ol>
<li>Nainstalujte si, prosím, [Windows Mixed Reality Portal](https://www.microsoft.com/en-us/p/mixed-reality-portal/9ng1h8b3zc7m#activetab=pivot:overviewtab).</li>
<li>Dále si nainstalujte [Steam](https://store.steampowered.com/) a rozšíření [SteamVR](https://store.steampowered.com/app/250820/SteamVR/).</li>
<li>Zapněte obě aplikace, připojte headset a kontroler.</li>
<li>Stáhněte si tento repozitář.</li>
<li>Nainstalujte si [Unity verze 2020.1.17f1](https://unity3d.com/get-unity/download)</li>
<li>Ve složce [bp-koudelkova/Langweil_MMP_MockupRoom/Build](https://gitlab.fel.cvut.cz/koudebar/bp-koudelkova/-/tree/main/Langweil_MMP_MockupRoom/Build) spusťte .exe soubor</li>
</ol>

## Ovládání aplikace
<ol>
<li>Spusťte aplikaci.</li>
<li>Pomocí pravého kontroleru vyberte požadovanou kapitolu, potvrďte stiskem GrabPinch.</li>
<li>Zapne se přehrávání, pokud si přejete kapitolu změnit, stiskněte GrabGrip. Opětovné stisknutí tlačítka schová dialogové okno.</li>
<li>V průběhu projekce se lze teleportovat a otáčet po scéně pomocí stisknutí páčky joysticku dopředu.</li>
<li> PRO UKONČENÍ přehrávání stiskněte tlačítko Ukončit aplikaci.</li>
